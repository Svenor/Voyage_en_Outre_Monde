Voyage en Outre Monde
===============

Description
-----------

*Voyage en Outre Monde* est une extension sous forme de campagne pour le jeu *La Bataille pour Wesnoth*. Cette extension ajoute quelques scénarios, et une histoire originale.
Cette campagne avait pour but de me servir d'entraînement au WML (Wesnoth Markup Language), mais est devenue un véritable projet.

Autres informations
-------------------
L'auteur principal de cette campagne est [Svenor](https://gitlab.com/Svenor/). L'adresse web du dépôt sur Gitlab est https://gitlab.com/Svenor/Voyage_en_Outre_Monde. Vous pouvez me contacter sur le Discord officiel du jeu (@Svenor).

Utilisation
-----------
La campagne n'est pas encore disponible sur le serveur d'extensions du jeu. 
Voici la marche à suivre pour l'installer depuis ce dépôt :
1. Installer [git](https://git-scm.com/) sur votre ordinateur ([Documentation de git](https://git-scm.com/doc))
2. Cloner le dépôt (https://gitlab.com/Svenor/Voyage_en_Outre_Monde.git) à l'intérieur du dossier *userdata/data/add-ons/* de Wesnoth (voir [cette page](https://wiki.wesnoth.org/EditingWesnoth) - [version française](https://wiki.wesnoth.org/EditingWesnoth/fr))
3. Lancer le jeu, la campagne devrait être disponible dans la liste des campagnes (au même endroit que les campagnes d'extensions téléchargées sur le serveur d'extension)

Report ou bug
-------------
Pour me prévenir d'un bug, plusieurs solutions s'offrent à vous :
* Ouvrir un *Issue* sur le dépôt Gitlab (plus simple à gérer)
* Me contacter sur Discord (Svenor#4487)
* Me contacter par mail, via Gitlab